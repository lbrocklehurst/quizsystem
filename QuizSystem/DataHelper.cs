﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using QuizSystem;

namespace QuizSystem
{
    public class DataHelper
    {
        public static string[] FoldersInDirectory(string dir)
        {
            return Directory.GetDirectories(dir);
        }

        public static string[] FindFilesInFolder(string folder)
        {
            return Directory.GetFiles(folder, "*.txt");
        }

        static IEnumerable<string> ReadAsLines(string filename)
        {
            using (StreamReader reader = new StreamReader(filename))
                while (!reader.EndOfStream)
                    yield return reader.ReadLine();
        }

        /// <summary>
        /// Read from a tab deliminated excel file
        /// </summary>
        public static Question[] ReadExcelFile(string filename)
        {
            IEnumerable<string> reader = ReadAsLines(filename);

            if (reader == null)
                return null;

            IEnumerable<string> rows = reader.Skip(1); // This assumes the first record is filled with the column names

            List<Question> questions = new List<Question>();

            foreach (string fields in rows)
            {
                string[] entries = fields.Split('\t');
                if (entries.Length < Globals.ColumnLength)
                {
                    Debug.WriteLine(string.Format("Row is at wrong length ({0}) - skipping.", entries.Length));
                    continue;
                }
                //TODO: TryParse checks
                Question question = new Question();
                question.ID = int.Parse(entries[0]);
                question.Active = (entries[1].Equals("Y")) ? true : false;
                question.QuestionText = entries[2];

                // Answers
                List<string> answers = new List<string>();
                for (int i = 3; i <= 6; i++)
                    if (entries[i] != null && entries[i] != "")
                        answers.Add(entries[i]);
                question.Answers = answers.ToArray();

                question.CorrectAnswer = int.Parse(entries[7]) - 1;
                question.Category = entries[8];

                questions.Add(question);
            }
            return questions.ToArray();
        }
        public static Question[] ReadExcelFile(string location, string name)
        {
            string filename = Path.Combine(location, name);
            return ReadExcelFile(filename);
        }

        /// <summary>
        /// Save Question Array to binary
        /// </summary>
        public static bool SaveToBinary(string filelocation, Question[] questions)
        {
            try
            {
                using (Stream stream = File.Open(filelocation, FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, questions);
                    stream.Close();
                }
                return true;
            }
            catch (IOException ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }
        public static bool SaveToBinary(string location, string name, Question[] questions)
        {
            string filelocation = Path.Combine(location, name);
            return SaveToBinary(filelocation, questions);
        }

        /// <summary>
        /// Load Question Array from binary file
        /// </summary>
        public static Question[] LoadFromBinary(string filelocation)
        {
            if (File.Exists(filelocation))
            {
                Question[] questions;
                try
                {
                    using (Stream stream = File.Open(filelocation, FileMode.Open))
                    {
                        BinaryFormatter bin = new BinaryFormatter();

                        questions = (Question[])bin.Deserialize(stream);
                        stream.Close();

                        return questions;
                    }
                }
                catch (IOException ex)
                {
                    throw new ArgumentException(ex.ToString());
                }
            }
            else
            {
                Debug.WriteLine("Load failed, '{0}' not found", filelocation);
                return null;
            }
        }
        public static Question[] LoadFromBinary(string location, string name)
        {
            string filelocation = Path.Combine(location, name);
            return LoadFromBinary(filelocation);
        }
        /// <summary>
        /// Save Quiz file
        /// </summary>
        public static bool SaveQuiz(Quiz quiz, string filelocation)
        {
            try
            {
                using (Stream stream = File.Open(filelocation, FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, quiz);
                    stream.Close();
                    File.SetAttributes(filelocation, FileAttributes.Hidden);
                    SetFileReadAccess(filelocation, true);
                }
                return true;
            }
            catch (IOException ex)
            {
                throw new ArgumentException(ex.ToString());
            }
        }
        public static bool SaveQuiz(Quiz quiz, string location, string name)
        {
            string filelocation = Path.Combine(location, name);
            return SaveQuiz(quiz, filelocation);
        }

        /// <summary>
        /// Load Quiz File
        /// </summary>
        public static Quiz LoadQuiz(string filelocation)
        {
            if (File.Exists(filelocation))
            {
                Quiz quiz;
                try
                {
                    using (Stream stream = File.Open(filelocation, FileMode.Open, FileAccess.Read))
                    {
                        BinaryFormatter bin = new BinaryFormatter();

                        quiz = (Quiz)bin.Deserialize(stream);
                        stream.Close();

                        return quiz;
                    }
                }
                catch (IOException ex)
                {
                    throw new ArgumentException(ex.ToString());
                }
            }
            else
            {
                Debug.WriteLine("Load failed, '{0}' not found", filelocation);
                return null;
            }
        }
        public static Quiz LoadQuiz(string location, string name)
        {
            string filelocation = Path.Combine(location, name);
            return LoadQuiz(filelocation);
        }

        public static void SetFileReadAccess(string FileName, bool SetReadOnly)
        {
            FileInfo fInfo = new FileInfo(FileName);

            // Set the IsReadOnly property.
            fInfo.IsReadOnly = SetReadOnly;

        }

    }
}
