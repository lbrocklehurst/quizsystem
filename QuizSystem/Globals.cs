﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizSystem
{
    public class Globals
    {
        public const int ColumnLength = 9;
        public const string ServerLocation = @"N:\RSGLIN\General Documents\QuizSystem",
            QuizFolder = "Quizzes", ResultsFolder = "Results",
            FileExt = "txt";
    }
}
