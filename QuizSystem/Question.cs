﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizSystem
{
    [Serializable]
    public class Question
    {
        public int ID { get; set; }
        public int CorrectAnswer { get; set; }
        public int ChosenAnswer = -1;
        public bool Active { get; set; }
        public string QuestionText { get; set; }
        public string Category { get; set; }
        public string[] Answers { get; set; }
    }
}
