﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizSystem
{
    [Serializable]
    public class Quiz
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Category { get; set; }
        public Question[] Questions { get; set; }
    }
}
