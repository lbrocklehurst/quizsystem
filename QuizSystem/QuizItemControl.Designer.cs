﻿namespace QuizSystem
{
    partial class QuizItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblQuestionTitle = new System.Windows.Forms.Label();
            this.answerButton0 = new System.Windows.Forms.RadioButton();
            this.answerButton1 = new System.Windows.Forms.RadioButton();
            this.answerButton2 = new System.Windows.Forms.RadioButton();
            this.answerButton3 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // lblQuestionTitle
            // 
            this.lblQuestionTitle.AutoSize = true;
            this.lblQuestionTitle.Location = new System.Drawing.Point(5, 5);
            this.lblQuestionTitle.Name = "lblQuestionTitle";
            this.lblQuestionTitle.Size = new System.Drawing.Size(72, 13);
            this.lblQuestionTitle.TabIndex = 0;
            this.lblQuestionTitle.Text = "Question Title";
            // 
            // answerButton0
            // 
            this.answerButton0.AutoSize = true;
            this.answerButton0.Location = new System.Drawing.Point(8, 30);
            this.answerButton0.Name = "answerButton0";
            this.answerButton0.Size = new System.Drawing.Size(96, 17);
            this.answerButton0.TabIndex = 2;
            this.answerButton0.TabStop = true;
            this.answerButton0.Text = "answerButton0";
            this.answerButton0.UseVisualStyleBackColor = true;
            // 
            // answerButton1
            // 
            this.answerButton1.AutoSize = true;
            this.answerButton1.Location = new System.Drawing.Point(8, 53);
            this.answerButton1.Name = "answerButton1";
            this.answerButton1.Size = new System.Drawing.Size(96, 17);
            this.answerButton1.TabIndex = 3;
            this.answerButton1.TabStop = true;
            this.answerButton1.Text = "answerButton1";
            this.answerButton1.UseVisualStyleBackColor = true;
            // 
            // answerButton2
            // 
            this.answerButton2.AutoSize = true;
            this.answerButton2.Location = new System.Drawing.Point(8, 76);
            this.answerButton2.Name = "answerButton2";
            this.answerButton2.Size = new System.Drawing.Size(96, 17);
            this.answerButton2.TabIndex = 3;
            this.answerButton2.TabStop = true;
            this.answerButton2.Text = "answerButton2";
            this.answerButton2.UseVisualStyleBackColor = true;
            // 
            // answerButton3
            // 
            this.answerButton3.AutoSize = true;
            this.answerButton3.Location = new System.Drawing.Point(8, 99);
            this.answerButton3.Name = "answerButton3";
            this.answerButton3.Size = new System.Drawing.Size(96, 17);
            this.answerButton3.TabIndex = 3;
            this.answerButton3.TabStop = true;
            this.answerButton3.Text = "answerButton3";
            this.answerButton3.UseVisualStyleBackColor = true;
            // 
            // QuizItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.answerButton3);
            this.Controls.Add(this.answerButton2);
            this.Controls.Add(this.answerButton1);
            this.Controls.Add(this.answerButton0);
            this.Controls.Add(this.lblQuestionTitle);
            this.Name = "QuizItemControl";
            this.Size = new System.Drawing.Size(111, 125);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblQuestionTitle;
        private System.Windows.Forms.RadioButton answerButton0;
        private System.Windows.Forms.RadioButton answerButton1;
        private System.Windows.Forms.RadioButton answerButton2;
        private System.Windows.Forms.RadioButton answerButton3;
    }
}
