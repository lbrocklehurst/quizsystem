﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizSystem
{
    public partial class QuizItemControl : UserControl
    {
        RadioButton[] answerButtons = new RadioButton[4];
        public QuizItemControl()
        {
            InitializeComponent();
        }
        public QuizItemControl(Question question)
        {
            InitializeComponent();

            answerButtons[0] = answerButton0;
            answerButtons[1] = answerButton1;
            answerButtons[2] = answerButton2;
            answerButtons[3] = answerButton3;

            lblQuestionTitle.Text = question.QuestionText;

            for (int i = 0; i < answerButtons.Length; i++)
            {
                if (i < question.Answers.Length)
                {
                    answerButtons[i].Show();
                    answerButtons[i].Text = question.Answers[i];
                }
                else
                    answerButtons[i].Hide();
            }
        }
        /// <summary>
        /// Select a desired answer
        /// </summary>
        public void SelectAnswer(int index)
        {
            answerButtons[index].Checked = true;
        }
        /// <summary>
        /// Retrieve the currently selected answer
        /// </summary>
        public int SelectedAnswer()
        {
            int selected = -1;
            for(int i = 0; i < answerButtons.Length; i++)
            {
                if (answerButtons[i].Checked)
                {
                    selected = i;
                    break;
                }
            }
            return selected;
        }
    }
}
