﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizSystem
{
    public partial class QuizSystemForm : Form
    {
        Quiz CurrentQuiz;
        int currentQuestion = 0;

        QuizItemControl currentQuizItem;

        public QuizSystemForm()
        {
            InitializeComponent();
        }
        public QuizSystemForm(Quiz quiz)
        {
            CurrentQuiz = quiz;
            InitializeComponent();
        }

        private void QuizSystemForm_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("Quiz: {0} - {1}", CurrentQuiz.Name, CurrentQuiz.Category);
            LoadCurrentQuestion();
        }

        private void LoadCurrentQuestion()
        {
            lblQuestionNum.Text = string.Format("{0}/{1}", currentQuestion + 1, CurrentQuiz.Questions.Length);
            if (currentQuestion >= CurrentQuiz.Questions.Length)
                return;
            mainPanel.Controls.Clear();
            currentQuizItem = new QuizItemControl(CurrentQuiz.Questions[currentQuestion]);
            int selectedAnswer = CurrentQuiz.Questions[currentQuestion].ChosenAnswer;
            if (selectedAnswer >= 0)
                currentQuizItem.SelectAnswer(selectedAnswer);
            mainPanel.Controls.Add(currentQuizItem);
        }



        private void btnNext_Click(object sender, EventArgs e)
        {
            int selected = currentQuizItem.SelectedAnswer();
            if(selected < 0)
            {
                MessageBox.Show("Please select an answer!", "No answer selected.", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            CurrentQuiz.Questions[currentQuestion].ChosenAnswer = selected;
            currentQuestion++;

            if (currentQuestion >= CurrentQuiz.Questions.Length)
                FinishedQuiz();

            LoadCurrentQuestion();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            int selected = currentQuizItem.SelectedAnswer();
            if(selected >= 0)   // Mark selected answer, if needed
                CurrentQuiz.Questions[currentQuestion].ChosenAnswer = selected;
            currentQuestion--;
            if(currentQuestion < 0)
            {
                currentQuestion = 0;
                return;
            }
            LoadCurrentQuestion();
        }

        private void FinishedQuiz()
        {
            int amountCorrect = 0;
            foreach (Question question in CurrentQuiz.Questions)
            {
                if (question.ChosenAnswer == question.CorrectAnswer)
                    amountCorrect++;
            }

            MessageBox.Show(string.Format("Amount Correct: {0}/{1} ({2}%)", amountCorrect, CurrentQuiz.Questions.Length,
                ((float)amountCorrect / (float)CurrentQuiz.Questions.Length) * 100
                ));

            SaveResults();

            this.Close();
        }

        private void SaveResults()
        {
            string folder = Path.Combine(Globals.ServerLocation, Globals.ResultsFolder, CurrentQuiz.Name);
            string filename = string.Format("{0}_{1}_{2}.{3}", CurrentQuiz.Name, CurrentQuiz.Category, CurrentQuiz.Username, Globals.FileExt);
            try
            {
                if (Directory.Exists(folder))
                {
                    Debug.WriteLine("Folder exists already.");
                    DataHelper.SaveQuiz(CurrentQuiz, folder, filename);
                    return;
                }

                DirectoryInfo di = Directory.CreateDirectory(folder);
                Debug.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(folder));
                DataHelper.SaveQuiz(CurrentQuiz, folder, filename);
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
            finally { }
        }

    }
}
