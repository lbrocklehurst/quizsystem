﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizSystem
{
    public partial class SelectionForm : Form
    {
        List<Quiz> QuizList = new List<Quiz>();

        public SelectionForm()
        {
            InitializeComponent();
        }

        private void SelectionForm_Load(object sender, EventArgs e)
        {
            GetQuizList();
            GetCategoryList();
        }
        /// <summary>
        /// Gather and display a list of Quizzes within server folder
        /// </summary>
        private void GetQuizList()
        {
            comboQuizPick.Items.Clear();
            QuizList.Clear();

            string quizfolder = Path.Combine(Globals.ServerLocation, Globals.QuizFolder);

            if (!Directory.Exists(quizfolder))
                Directory.CreateDirectory(quizfolder);

            string[] files = DataHelper.FindFilesInFolder(quizfolder);  // Find quiz files within Server folder
            foreach(string file in files)   // Loop through files
            {
                Question[] questions = DataHelper.ReadExcelFile(file);  // Read questions
                if (questions == null || questions.Length <= 0)
                    continue;   // Failed, skip.

                string quizName = Path.GetFileNameWithoutExtension(file);   // Generate name from filename
                comboQuizPick.Items.Add(quizName);  // Add to ComboBox & QuizList
                QuizList.Add(new Quiz()
                {
                    Name = quizName,
                    Questions = questions
                });
            }
        }
        /// <summary>
        /// Generate a list of Categories from the currently selected Quiz
        /// </summary>
        private void GetCategoryList()
        {
            if (comboQuizPick.SelectedIndex < 0)
                return;

            comboCategory.Items.Clear();    // Clear existing
            Quiz quiz = SelectedQuiz; // Find Quiz from list

            foreach (Question question in quiz.Questions)   // Loop through quiz and pull out Categories
            {
                if (!comboCategory.Items.Contains(question.Category))   // If not already there, add.
                    comboCategory.Items.Add(question.Category);
            }
        }
        /// <summary>
        /// Start Quiz button pressed.
        /// </summary>
        private void btnStart_Click(object sender, EventArgs e)
        {
            // Check for empty Fields/ComboBoxes
            if (comboQuizPick.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a Quiz!", "Didn't select a Quiz", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (txtUsername.Text == "")
            {
                MessageBox.Show("Please enter a username!", "No Username set", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (comboCategory.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a Category!", "Didn't select a Category", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            // Bundle up quiz
            Quiz currentQuiz = new Quiz();
            currentQuiz.Name = SelectedQuiz.Name;   // Add name
            currentQuiz.Username = txtUsername.Text;
            currentQuiz.Category = (string)comboCategory.Items[comboCategory.SelectedIndex];    // Get Category name
            currentQuiz.Questions = GetCategoryQuestions(currentQuiz.Category); // Get questions based on Category

            // Present Quiz window
            this.Hide();
            QuizSystemForm quizform = new QuizSystemForm(currentQuiz);
            quizform.ShowDialog();
            this.Close();
        }

        private Question[] GetCategoryQuestions(string category)
        {
            Quiz quiz = SelectedQuiz; // Find currently selected Quiz

            var categoryquestions =
                from item in quiz.Questions
                where item.Category == category
                && item.Active == true
                select item;

            return categoryquestions.ToArray();
        }

        private Quiz SelectedQuiz
        {
            get { return QuizList.Find(x => x.Name == (string)comboQuizPick.Items[comboQuizPick.SelectedIndex]); }
        }

        private void comboQuizPick_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCategoryList();
        }
    }
}
