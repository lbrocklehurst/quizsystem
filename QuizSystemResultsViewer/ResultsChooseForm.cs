﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuizSystem;
using System.IO;
using System.Diagnostics;

namespace QuizSystemResultsViewer
{
    public partial class ResultsChooseForm : Form
    {
        string allCategories = "<ALL>";

        public ResultsChooseForm()
        {
            InitializeComponent();
        }

        string QuizFolder
        {
            get { return Path.Combine(Globals.ServerLocation, Globals.ResultsFolder, (string)comboQuiz.Items[comboQuiz.SelectedIndex]); }
        }

        private void ResultsForm_Load(object sender, EventArgs e)
        {
            GetQuizList();
        }

        private void GetQuizList()
        {
            comboQuiz.Items.Clear();

            string resultsfolder = Path.Combine(Globals.ServerLocation, Globals.ResultsFolder);

            if (!Directory.Exists(resultsfolder))
                Directory.CreateDirectory(resultsfolder);

            string[] quizfolders = DataHelper.FoldersInDirectory(resultsfolder);
            foreach(string folder in quizfolders)
            {
                string quizName = Path.GetFileName(folder);
                comboQuiz.Items.Add(quizName);
            }
        }

        private void GetCategoryList()
        {
            comboCategory.Items.Clear();
            comboCategory.Items.Add(allCategories);

            string[] files = DataHelper.FindFilesInFolder(QuizFolder);  // Find Quiz files within folder
            foreach (string file in files)   // Loop through files
            {
                Quiz quiz = DataHelper.LoadQuiz(file);  // Load Quiz
                if (quiz == null)
                    continue;   // Failed, skip.

                if (!comboCategory.Items.Contains(quiz.Category))
                    comboCategory.Items.Add(quiz.Category);  // Add to ComboBox
            }
            comboCategory.SelectedIndex = 0;
        }

        private void LoadResults()
        {
            List<Quiz> completedQuizzes = new List<Quiz>();

            string[] files = DataHelper.FindFilesInFolder(QuizFolder);
            string categoryFilter = (string)comboCategory.Items[comboCategory.SelectedIndex];

            foreach(string file in files)
            {
                Quiz quiz = DataHelper.LoadQuiz(file);  // Load Quiz
                if (quiz == null)
                    continue;   // Failed, skip.

                if (categoryFilter.Equals(allCategories) || quiz.Category.Equals(categoryFilter))
                    completedQuizzes.Add(quiz);  // Add to ComboBox
            }

            // Present Quiz window
            this.Hide();
            ResultsForm resultsform = new ResultsForm(completedQuizzes);
            resultsform.ShowDialog();
            this.Close();
        }

        private void comboQuiz_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboQuiz.SelectedIndex < 0)
                return;
            GetCategoryList();
        }

        private void btnGetResults_Click(object sender, EventArgs e)
        {
            LoadResults();
        }
    }
}
