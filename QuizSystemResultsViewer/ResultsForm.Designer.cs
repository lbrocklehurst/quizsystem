﻿namespace QuizSystemResultsViewer
{
    partial class ResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableResults = new System.Windows.Forms.DataGridView();
            this.colUsername = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuestion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAnswered = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCorrect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tableResults)).BeginInit();
            this.SuspendLayout();
            // 
            // tableResults
            // 
            this.tableResults.AllowUserToAddRows = false;
            this.tableResults.AllowUserToOrderColumns = true;
            this.tableResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.tableResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableResults.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUsername,
            this.colCategory,
            this.colQuestion,
            this.colAnswered,
            this.colCorrect});
            this.tableResults.Location = new System.Drawing.Point(12, 12);
            this.tableResults.Name = "tableResults";
            this.tableResults.RowHeadersVisible = false;
            this.tableResults.Size = new System.Drawing.Size(784, 316);
            this.tableResults.TabIndex = 0;
            // 
            // colUsername
            // 
            this.colUsername.HeaderText = "Username";
            this.colUsername.Name = "colUsername";
            this.colUsername.ReadOnly = true;
            this.colUsername.Width = 80;
            // 
            // colCategory
            // 
            this.colCategory.HeaderText = "Category";
            this.colCategory.Name = "colCategory";
            this.colCategory.ReadOnly = true;
            this.colCategory.Width = 74;
            // 
            // colQuestion
            // 
            this.colQuestion.HeaderText = "Question";
            this.colQuestion.Name = "colQuestion";
            this.colQuestion.ReadOnly = true;
            this.colQuestion.Width = 74;
            // 
            // colAnswered
            // 
            this.colAnswered.HeaderText = "Answered";
            this.colAnswered.Name = "colAnswered";
            this.colAnswered.ReadOnly = true;
            this.colAnswered.Width = 79;
            // 
            // colCorrect
            // 
            this.colCorrect.HeaderText = "Correct?";
            this.colCorrect.Name = "colCorrect";
            this.colCorrect.ReadOnly = true;
            this.colCorrect.Width = 53;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(689, 334);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(107, 23);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "Open in Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // ResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 369);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.tableResults);
            this.Name = "ResultsForm";
            this.Text = "Results";
            this.Load += new System.EventHandler(this.ResultsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tableResults)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tableResults;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUsername;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuestion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAnswered;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colCorrect;
        private System.Windows.Forms.Button btnExport;
    }
}