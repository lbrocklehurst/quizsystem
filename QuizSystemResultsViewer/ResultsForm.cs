﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuizSystem;

namespace QuizSystemResultsViewer
{
    public partial class ResultsForm : Form
    {
        List<Quiz> Results = new List<Quiz>();

        public ResultsForm()
        {
            InitializeComponent();
        }
        public ResultsForm(List<Quiz> results)
        {
            this.Results = results;

            InitializeComponent();
        }

        private void ResultsForm_Load(object sender, EventArgs e)
        {
            AddData();
        }
        
        private void AddData()
        {
            foreach(Quiz quiz in Results)
            {
                for (int i = 0; i < quiz.Questions.Length; i++)
                {
                    tableResults.Rows.Add(
                        (i == 0) ? quiz.Username : "",
                        quiz.Category,
                        quiz.Questions[i].QuestionText,
                        (quiz.Questions[i].ChosenAnswer + 1).ToString(),
                        (quiz.Questions[i].ChosenAnswer == quiz.Questions[i].CorrectAnswer)
                    );
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportTableToExcel();
        }

        private void ExportTableToExcel()
        {
            CopyTableToClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true); 
        }

        private void CopyTableToClipboard()
        {
            tableResults.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            tableResults.MultiSelect = true;
            tableResults.SelectAll();

            DataObject dataObj = tableResults.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

    }
}
